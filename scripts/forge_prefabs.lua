--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

require("forge_prefab")
return {
	-- Arenas
	"lavaarena",
	"reforged_tiles_arena",

	-- Mobs
	"pitpig",
	"boarilla",
	"snortoise",
	"scorpeon",
	"crocommander",
	"boarrior",
	"rhinocebro",
	"swineclops",
	"groundlifts",
	"battlestandards",

	-- Pets
	"golem",
	"babyspider",
	"forge_abigail",
	"forge_abigail_flower",
	"forge_bernie",
	"mean_flytrap",
	"adult_flytrap",
	"forge_cookpot",
	"merm_guard",
	"forge_woby",
	"buff_woby",
	"baby_ben",

	-- Weapons
	"forgedarts",
	"forginghammer",
	"riledlucy",
	"pithpike",
	"livingstaff",
	"moltendarts",
	"infernalstaff",
	"spiralspear",
	"firebomb",
    "blacksmithsedge",
	"forge_books",
	"gauntlet",
	"teleport_staff",
	"forge_trident",
	"lavaarena_seeddart",
	"lavaarena_seeddart2",
	"lavaarena_spatula",
	"lavaarena_chefhat",
	"forge_slingshot",
	"spice_bomb",
	"pocketwatch_reforged",
	"portal_staff",
	"balloons_reforged",

	-- Armor
	"forge_armor",
	"forge_hats",

	-- Projectiles
	"forge_fireball_projectile",
	"infernalstaff_meteor",
	"scorpeon_projectile",

	-- Debuffs
	"debuff_poison",
	"debuff_pollen",
	"debuff_wet",
	"debuff_haunt",
	"debuff_armorbreak",
	"debuff_mfd",
	"debuff_fire",
	"debuff_spice",
	"debuff_shield",

	-- Trails
	"ground_trail",
	"poison_trail",

	-- FX
	--"weaponsparks_fx",
	--"forgedebuff_fx",
	--"forgespear_fx",
	"healingcircle",
	"healingcircle_regenbuff",
	--"passive_battlecry_fx",
	"passive_shadow_fx",
	"forge_fx", -- TODO testing, might move all fx into this file
	"forge_wortox_soul_fx",
	"fossilized_break_fx",

	-- Other
	"damage_number",
	"geyser",
	"fissures",
	"reforged_deps",
	"forge_reticules",
	"forge_traps",
	"spectator",
	"ping_banners",
	"rf_generic_statuses",
	"reforged_talkers",
	"rf_map_blockers",
}

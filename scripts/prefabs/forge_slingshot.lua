--[[
Copyright (C) 2018 Forged Forge

This file is part of Forged Forge.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]
--[[
TODO
	when using barrage in a crowd of enemies, enemies behind the character may get hit by the darts
	dart spread is incorrect for barrage
--]]
local SLINGSHOTPART_DEFS = require("prefabs/slingshotpart_defs")

local assets = {
    Asset("ANIM", "anim/slingshot.zip"),
    Asset("ANIM", "anim/floating_items.zip"),
	Asset("INV_IMAGE", "slingshot"),
	Asset("INV_IMAGE", "slingshot_body"),
	Asset("INV_IMAGE", "slingshot_band_back"),
	Asset("INV_IMAGE", "slingshot_band_front"),
	Asset("SCRIPT", "scripts/prefabs/slingshotpart_defs.lua"),
}
local assets_projectile = {
    Asset("ANIM", "anim/lavaarena_blowdart_attacks.zip"),
	Asset("ANIM", "anim/forge_slingshot_projectile.zip"),
	Asset("ANIM", "anim/slingshotammo.zip"),
}
local prefabs = {
    "forgedarts_projectile",
    "forgedarts_projectile_alt",
    "reticulelongmulti",
    "reticulelongmultiping",
}
local prefabs_projectile = {
    "weaponsparks_piercing_fx",
}
local tuning_values = TUNING.FORGE.FORGE_SLINGSHOT
--------------------------------------------------------------------------
-- Attack Functions
--------------------------------------------------------------------------
local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .4, .02, .2, inst, 30)
end

local function OnHit_Alt(inst, attacker, target)
    local explosion_fx = COMMON_FNS.CreateFX("explosivehit", target, attacker)
    explosion_fx.Transform:SetPosition(inst:GetPosition():Get())
	ShakeIfClose(inst)
	if inst.owner then
        local scale = inst.Transform:GetScale()
		local targets = COMMON_FNS.EQUIPMENT.GetAOETargets(attacker, inst:GetPosition(), tuning_values.ALT_RADIUS*scale, {"_combat", "LA_mob"}, {"player", "companion", "notarget"})
		inst.owner.components.weapon:DoAltAttack(attacker, targets, inst, "explosive")
	end
	if target and target.components.debuffable then
		target.components.debuffable:AddDebuff("debuff_mfd", "debuff_mfd")
	end
	inst:Remove()
end
--------------------------------------------------------------------------
-- Ability Functions
--------------------------------------------------------------------------
local function Powershot(inst, caster, pos)
	local damage = caster.components.combat:CalcDamage(nil, inst, nil, true)
	local proj = SpawnPrefab("forge_slingshot_projectile_alt")
	proj.owner = inst
	proj.Transform:SetPosition(inst:GetPosition():Get())
	proj.components.projectile:AimedThrow(inst, caster, pos, 0, true)
	proj.components.projectile:SetOnHitFn(OnHit_Alt)
	--caster.SoundEmitter:PlaySound("dontstarve/common/lava_arena/blow_proj_spread")
	inst.components.rechargeable:StartRecharge()
	inst.components.aoespell:OnSpellCast(caster, nil, proj)
end
--------------------------------------------------------------------------
-- Weapon Functions
--------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------
--For layered inventory icons

local PART_NAMES =
{
	band = {},
	frame = {},
	handle = {},
}
for k, v in pairs(SLINGSHOTPART_DEFS) do
	table.insert(PART_NAMES[v.slot], k)
end

local PART_IDS = {}
for k, v in pairs(PART_NAMES) do
	PART_IDS[k] = table.invert(v)
end

local function _AddLayer(tbl, idx, name)
	local row = tbl[idx]
	if row then
		row.image = name..".tex"
	else
		tbl[idx] = { image = name..".tex" }
	end
	return idx + 1
end

local function OnIconDirty(inst)
	local band = PART_NAMES.band[inst.bandid:value()]
	local frame = PART_NAMES.frame[inst.frameid]
	local handle = PART_NAMES.handle[inst.handleid:value()]
	if band or frame or handle then
		if inst._iconlayers == nil then
			inst._iconlayers = {}
		end
		local build = inst.buildname:value()
		if string.len(build) <= 0 then
			build = "slingshot"
		end
		local j = 1
		j = _AddLayer(inst._iconlayers, j, band and (band.."_back") or (build.."_band_back"))
		j = _AddLayer(inst._iconlayers, j, build.."_body")
		j = _AddLayer(inst._iconlayers, j, band and (band.."_front") or (build.."_band_front"))
		if handle then
			j = _AddLayer(inst._iconlayers, j, handle.."_ol")
		end
		if frame then
			j = _AddLayer(inst._iconlayers, j, frame.."_ol")
		end	
		for i = j, #inst._iconlayers do
			inst._iconlayers[i] = nil
		end
	else
		inst._iconlayers = nil --use default inv img
	end
	inst:PushEvent("imagechange")
end

local function SetBandIcon(inst, name)
	local id = PART_IDS.band[name] or 0
	if inst.bandid:value() ~= id then
		inst.bandid:set(id)
		OnIconDirty(inst)
	end
end

local function SetFrameIcon(inst, name)
	local id = PART_IDS.frame[name] --or 0
	if inst.frameid ~= id then
		inst.frameid = id
		OnIconDirty(inst)
	end
end

local function SetHandleIcon(inst, name)
	local id = PART_IDS.handle[name] or 0
	if inst.handleid:value() ~= id then
		inst.handleid:set(id)
		OnIconDirty(inst)
	end
end

local function LayeredInvImageFn(inst)
	return inst._iconlayers
end

local function OnSlingshotSkinChanged(inst, skin_build)
	inst.buildname:set(skin_build or "")
	inst.components.inventoryitem:ChangeImageName(skin_build or "slingshot")
	OnIconDirty(inst)
end
-----------------------------------------------------------
local function OnRemoveFx(fx)
	local parent = fx._highlightparent
	if parent and parent.highlightchildren then
		table.removearrayvalue(parent.highlightchildren, fx)
	end
end

local function SetHighlightChildren(fx, parent)
	if parent.highlightchildren then
		table.insert(parent.highlightchildren, fx)
	else
		parent.highlightchildren = { fx }
	end
	fx._highlightparent = parent
	fx.OnRemoveEntity = OnRemoveFx
end

local function CreateFollowFx(inst, anim, owner, frame1, frame2)
	local fx = SpawnPrefab("slingshotparts_fx")
	fx.entity:SetParent(owner.entity)
	fx.AnimState:PlayAnimation(anim)
	fx.Follower:FollowSymbol(owner.GUID, "swap_object", 0, 0, 0, true, false, frame1, frame2)
	if not TheNet:IsDedicated() then
		SetHighlightChildren(fx, owner)
	end
	if owner.components.colouradder then
		owner.components.colouradder:AttachChild(fx)
	end
	return fx
end

local function RefreshBand(inst, owner)
	local name, build, symbol = nil, nil, nil
	SetBandIcon(inst, name)
	if symbol then
		if inst.fx then
			for i, v in ipairs(inst.fx) do
				v.AnimState:OverrideSymbol("swap_band_top", build, symbol[1])
				v.AnimState:OverrideSymbol("swap_band_btm", build, symbol[2])
			end
		end
		if owner then
			owner.AnimState:OverrideSymbol("swap_band_btm", build, symbol[2])
		end
		inst.AnimState:OverrideSymbol("swap_band_top", build, symbol[1])
		inst.AnimState:OverrideSymbol("swap_band_btm", build, symbol[2])
	else
		local skin_build = inst:GetSkinBuild()
		if inst.fx then
			if skin_build then
				for i, v in ipairs(inst.fx) do
					v.AnimState:OverrideItemSkinSymbol("swap_band_top", skin_build, "swap_band_top", inst.GUID, "slingshot")
					v.AnimState:OverrideItemSkinSymbol("swap_band_btm", skin_build, "swap_band_btm", inst.GUID, "slingshot")
				end
			else
				for i, v in ipairs(inst.fx) do
					v.AnimState:ClearOverrideSymbol("swap_band_top")
					v.AnimState:ClearOverrideSymbol("swap_band_btm")
				end
			end
		end
		if owner then
			if skin_build then
				owner.AnimState:OverrideItemSkinSymbol("swap_band_btm", skin_build, "swap_band_btm", inst.GUID, "slingshot")
			else
				owner.AnimState:OverrideSymbol("swap_band_btm", "slingshot", "swap_band_btm")
			end
		end
		inst.AnimState:ClearOverrideSymbol("swap_band_top")
		inst.AnimState:ClearOverrideSymbol("swap_band_btm")
	end
end

local function RefreshFrame(inst)
	local name, build, symbol = nil, nil, nil
	--SetFrameIcon(inst, name) --frames shouldn't change after specific prefab is spawned
	if symbol then
		if inst.fx then
			for i, v in ipairs(inst.fx) do
				v.AnimState:OverrideSymbol("swap_frame", build, symbol)
			end
		end
		inst.AnimState:OverrideSymbol("swap_frame", build, symbol)
	else
		if inst.fx then
			for i, v in ipairs(inst.fx) do
				v.AnimState:ClearOverrideSymbol("swap_frame")
			end
		end
		inst.AnimState:ClearOverrideSymbol("swap_frame")
	end
end

local function RefreshHandle(inst)
	local name, build, symbol = nil, nil, nil
	SetHandleIcon(inst, name)
	if symbol then
		if inst.fx then
			for i, v in ipairs(inst.fx) do
				v.AnimState:OverrideSymbol("swap_handle", build, symbol)
			end
		end
		inst.AnimState:OverrideSymbol("swap_handle", build, symbol)
	else
		if inst.fx then
			for i, v in ipairs(inst.fx) do
				v.AnimState:ClearOverrideSymbol("swap_handle")
			end
		end
		inst.AnimState:ClearOverrideSymbol("swap_handle")
	end
end

local function OnEquip(inst, owner)
	if inst.fx then
		for i, v in ipairs(inst.fx) do
			v:Remove()
		end
	end
	inst.fx =
	{
		CreateFollowFx(inst, "swap_1_to_5", owner, 0, 5),
		CreateFollowFx(inst, "swap_6", owner, 5),
		CreateFollowFx(inst, "swap_7", owner, 6),
		CreateFollowFx(inst, "swap_8", owner, 7),
		CreateFollowFx(inst, "swap_9", owner, 8),
		CreateFollowFx(inst, "swap_17", owner, 16),
	}

    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("equipskinneditem", inst:GetSkinName())
		for i, v in ipairs(inst.fx) do
			v.AnimState:OverrideItemSkinSymbol("swap_slingshot", skin_build, "swap_slingshot", inst.GUID, "slingshot")
			v.AnimState:OverrideItemSkinSymbol("swap_band_top", skin_build, "swap_band_top", inst.GUID, "slingshot")
			v.AnimState:OverrideItemSkinSymbol("swap_band_btm", skin_build, "swap_band_btm", inst.GUID, "slingshot")
		end
		owner.AnimState:OverrideItemSkinSymbol("swap_band_btm", skin_build, "swap_band_btm", inst.GUID, "slingshot")
	else
		owner.AnimState:OverrideSymbol("swap_band_btm", "slingshot", "swap_band_btm")
    end
	owner.AnimState:OverrideSymbol("swap_object", "slingshot", "swap_empty")
	RefreshBand(inst, owner)
	RefreshFrame(inst)
	RefreshHandle(inst)
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")

    if owner.components.buffable then
        owner.components.buffable:AddBuff(tostring(inst.prefab) .. "_attack_speed", {{name = "attack_speed", type = "mult", val = 1.5}})
    end
end

local function OnUnequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
    owner.AnimState:ClearOverrideSymbol("swap_band_btm")

	if inst.fx then
		for i, v in ipairs(inst.fx) do
			v:Remove()
		end
		inst.fx = nil
	end

    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("unequipskinneditem", inst:GetSkinName())
    end

    if owner.components.buffable then
    	owner.components.buffable:RemoveBuff(tostring(inst.prefab) .. "_attack_speed")
    end
end
--------------------------------------------------------------------------
-- Pristine Functions
--------------------------------------------------------------------------
local function PristineFN(inst)
	COMMON_FNS.AddTags(inst, "slingshot", "aoeblowdart_long", "rangedweapon")
	inst.bandid = net_tinybyte(inst.GUID, "slingshot.bandid", "icondirty")
	inst.handleid = net_tinybyte(inst.GUID, "slingshot.handleid", "icondirty")
	inst.buildname = net_string(inst.GUID, "slingshot.buildname", "icondirty")
	inst.layeredinvimagefn = LayeredInvImageFn
end
--------------------------------------------------------------------------
local weapon_values = {
	name_override = "slingshot",
	swap_strings  = {"slingshot"},
	projectile    = "forge_slingshot_projectile",
	AOESpell      = Powershot,
    onequip_fn    = OnEquip,
    onunequip_fn  = OnUnequip,
	pristine_fn   = PristineFN,
}
--------------------------------------------------------------------------
local function fn()
	local inst = COMMON_FNS.EQUIPMENT.CommonWeaponFN("slingshot", nil, weapon_values, tuning_values)
	
	------------------------------------------
    if not TheWorld.ismastersim then
		--inst:ListenForEvent("icondirty", OnIconDirty)
        return inst
    end
	------------------------------------------
	inst.components.inventoryitem.atlasname = "images/inventoryimages3.xml"
	inst.components.inventoryitem.imagename = "slingshot"
    return inst
end
--------------------------------------------------------------------------
-- Projectile Functions
--------------------------------------------------------------------------
local function OnUpdateProjectileTail(inst)
    local tail = inst.CreateTail(inst.tail_values, inst)
    tail.Transform:SetPosition(inst.Transform:GetWorldPosition())
    tail.Transform:SetRotation(inst.Transform:GetRotation())
end

local function OnHit(inst, attacker, target)
    COMMON_FNS.CreateFX("weaponsparks_piercing_fx", target, attacker)
	inst:Remove()
end
--------------------------------------------------------------------------
local projectile_values = {
	speed         = 30,
	range         = tuning_values.HIT_RANGE,
	hit_dist      = 0.5,
	launch_offset = Vector3(0, 1, 0),
	OnHit         = OnHit,
	alt = {
		speed   = 25,
	},
}
local tail_values = {
    bank  = "forge_slingshot_projectile",
    build = "forge_slingshot_projectile",
    anim  = "trail",
    OnUpdateProjectileTail = OnUpdateProjectileTail,
}
--------------------------------------------------------------------------
local function commonprojectilefn(alt)
	projectile_values.pristine_fn = function(inst)
	    inst.entity:AddSoundEmitter()
	    ------------------------------------------
		if not alt then
			inst.AnimState:SetAddColour(0.5, 0.5, 0, 0)
		end
		------------------------------------------
	    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	end
	------------------------------------------
	local inst = COMMON_FNS.EQUIPMENT.CommonProjectileFN("forge_slingshot_projectile", "forge_slingshot_projectile", alt and "attack_alt" or "attack_idle", projectile_values, tail_values)
	------------------------------------------
    if not TheWorld.ismastersim then
        return inst
    end
    ------------------------------------------
    inst._hastail:set(true)
	------------------------------------------
	if alt then
		inst.components.projectile:SetRange(tuning_values.ALT_RANGE)
	end
	------------------------------------------
	inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/blow_dart")
	------------------------------------------
    return inst
end
--------------------------------------------------------------------------
local function projectilefn()
    return commonprojectilefn(false)
end

local function projectilealtfn()
    return commonprojectilefn(true)
end
--------------------------------------------------------------------------
return ForgePrefab("forge_slingshot", fn, assets, prefabs, nil, tuning_values.ENTITY_TYPE, nil, "images/inventoryimages3.xml", "slingshot.tex", "slingshot", "common_hand"),
    Prefab("forge_slingshot_projectile", projectilefn, assets_projectile, prefabs_projectile),
    Prefab("forge_slingshot_projectile_alt", projectilealtfn, assets_projectile, prefabs_projectile)

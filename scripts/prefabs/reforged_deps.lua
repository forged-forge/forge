local assets = {}
local prefabs = {
    -- Hard Mode
    "groundpound_fx",
    "groundpoundring_fx",
    "explode_small_slurtlehole",
}
return Prefab("reforged_deps", function() end, assets, prefabs)

The following assets are protected by the Law of Copyright:

lavaarena_buffindex.zip,
lavaarena_buff_build.zip,
lavaarena_debuff_build.zip,
lavaarena_bufficons.zip,

These assets are only allowed to be used by the game publisher, Klei Entertainment, and this modding team.
Any violation of this simple rule, aka uploading them to your own public mod, will result in a DMCA report and if it has to be done, a court visit.